<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Сегодняшняя дата</title>
</head>
<body>
  <p>Сегодняшняя дата (согласно данному веб-серверу):
    <?php
    echo date('l, F jS Y.');
    ?>
  </p>
  <p><?php echo 'Это <strong>проверка работы артифактов №3</strong>!'; ?></p>
</body>
</html>
